package com.gsafety.gstss.service.impl;

import com.gsafety.gstss.model.common.ReturnBase;
import com.gsafety.gstss.model.*;
import com.gsafety.gstss.mapper.CasePublicResponseMapper;
import com.gsafety.gstss.service.CasePublicResponseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(rollbackFor=Exception.class)
public class CasePublicResponseServiceImpl extends BaseServiceImpl<CasePublicResponse,CasePublicResponseMapper> implements CasePublicResponseService {
    @Override
    public ReturnBase<List<CasePublicResponse>> getByCase(String id) {
        return ReturnBase.succeed(baseMapper.getByCase(id));
    }
}




