package com.gsafety.gstss.service.impl;

import com.gsafety.gstss.model.common.ReturnBase;
import com.gsafety.gstss.model.*;
import com.gsafety.gstss.mapper.ExpertMapper;
import com.gsafety.gstss.service.ExpertService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@Transactional(rollbackFor=Exception.class)
public class ExpertServiceImpl extends BaseServiceImpl<Expert,ExpertMapper> implements ExpertService {

    @Override
    public ReturnBase<List<Expert>> getByKeyword(String keyword) {
        return ReturnBase.succeed(baseMapper.getByKeyword(keyword));
    }

    @Override
    public ReturnBase<List<Expert>> getByDomain(String domain) {
        return ReturnBase.succeed(baseMapper.getByDomain(domain));
    }

    @Override
    public ReturnBase<List<Expert>> getByDomainAndKeyword(String domain, String keyword) {
        Map map=new HashMap();
        map.put("domain",domain);
        map.put("keyword",keyword);
        return ReturnBase.succeed(baseMapper.getByDomainAndKeyword(map));
    }

    @Override
    public ReturnBase<List<String>> getDomains() {
        List<String> domains=baseMapper.getDomains();
        Set<String> set=new HashSet<String>();
        for (String domain:domains){
            String[] fields=domain.split(";");
            for (String field : fields){
                if(!set.contains(field))
                {
                    set.add(field);
                }
            }
        }
        domains= new ArrayList<String>(set);
        return ReturnBase.succeed(domains);
    }
}




