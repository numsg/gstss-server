package com.gsafety.gstss.service;

import com.gsafety.gstss.model.common.ReturnBase;
import com.gsafety.gstss.model.CaseCause;


import java.util.List;

public interface CaseCauseService  extends BaseService<CaseCause>{
    ReturnBase<List<CaseCause>> getByCase(String id);
}



