package com.gsafety.gstss.service.impl;

import com.gsafety.gstss.mapper.BaseMapper;
import com.gsafety.gstss.mapper.MissionRecordMapper;
import com.gsafety.gstss.model.MissionRecord;
import com.gsafety.gstss.model.common.ReturnBase;
import com.gsafety.gstss.service.BaseService;
import com.gsafety.gstss.service.MissionRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class MissionRecordServiceImpl extends BaseServiceImpl<MissionRecord,MissionRecordMapper> implements MissionRecordService {

    @Override
    public ReturnBase insert(List<MissionRecord> models) {
        for (MissionRecord model:models) {
            baseMapper.insert(model);
        }
        return ReturnBase.succeed();
    }


    @Override
    public ReturnBase<List<MissionRecord>> GetAllUnfinishedMission() {
        //MissionRecordMapper missionRecordMapper=(MissionRecordMapper)baseMapper;
        return ReturnBase.succeed(baseMapper.GetAllUnfinishedMission());
    }
}
