package com.gsafety.gstss.service;

import com.gsafety.gstss.model.common.ReturnBase;
import com.gsafety.gstss.model.CaseReference;


import java.util.List;

public interface CaseReferenceService  extends BaseService<CaseReference>{
    ReturnBase<List<String>> getIndicators();

    ReturnBase<List<CaseReference>> getByIndicator(String indicator);
}



