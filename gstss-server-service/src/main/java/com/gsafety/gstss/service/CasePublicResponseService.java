package com.gsafety.gstss.service;

import com.gsafety.gstss.model.common.ReturnBase;
import com.gsafety.gstss.model.CasePublicResponse;


import java.util.List;

public interface CasePublicResponseService  extends BaseService<CasePublicResponse>{
    ReturnBase<List<CasePublicResponse>> getByCase(String id);
}



