package com.gsafety.gstss.service;

import com.gsafety.gstss.model.common.ReturnBase;
import com.gsafety.gstss.model.CaseIndustryData;
import com.gsafety.gstss.model.misc.IndustryDataFilter;


import java.util.List;

public interface CaseIndustryDataService  extends BaseService<CaseIndustryData>{
    ReturnBase<List<CaseIndustryData>> getByFilter(IndustryDataFilter filter);

    ReturnBase<List<String>> getCategory();

    ReturnBase<List<String>> getCategoryByCategoryKeyword(String keyword);

    ReturnBase<List<String>> getCategoryByThemeKeyword(String keyword);

    ReturnBase<List<String>> getCategoryByIndicatorKeyword(String keyword);

    ReturnBase<List<String>> getCategoryByIndustryKeyword(String keyword);

    ReturnBase<List<String>> getTheme(String category);

    ReturnBase<List<String>> getThemeByThemeKeyword(String category, String keyword);

    ReturnBase<List<String>> getThemeByIndicatorKeyword(String category, String keyword);

    ReturnBase<List<String>> getThemeByIndustryKeyword(String category, String keyword);

    ReturnBase<List<String>> getIndicator(String category, String theme);

    ReturnBase<List<String>> getIndicatorByIndicatorKeyword(String category,String theme,String keyword);

    ReturnBase<List<String>> getIndicatorByIndustryKeyword(String category,String theme,String keyword);

    ReturnBase<List<String>> getIndustry(String category, String theme,String indicator);

    ReturnBase<List<String>> getIndustryByKeyword(String category, String theme, String indicator, String keyword);
}



