package com.gsafety.gstss.service.impl;

import com.gsafety.gstss.model.common.ReturnBase;
import com.gsafety.gstss.model.*;
import com.gsafety.gstss.mapper.CasePaperDataMapper;
import com.gsafety.gstss.service.CasePaperDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(rollbackFor=Exception.class)
public class CasePaperDataServiceImpl extends BaseServiceImpl<CasePaperData,CasePaperDataMapper> implements CasePaperDataService {

    @Override
    public ReturnBase<List<String>> getIndicators() {
        return ReturnBase.succeed(baseMapper.getIndicators());
    }

    @Override
    public ReturnBase<List<CasePaperData>> getByIndicator(String indicator) {
        return ReturnBase.succeed(baseMapper.getByIndicator(indicator));
    }
}




