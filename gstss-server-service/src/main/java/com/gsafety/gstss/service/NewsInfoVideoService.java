package com.gsafety.gstss.service;

import com.gsafety.gstss.model.common.ReturnBase;
import com.gsafety.gstss.model.NewsInfoVideo;


import java.util.List;

public interface NewsInfoVideoService  extends BaseService<NewsInfoVideo>{
    ReturnBase<List<NewsInfoVideo>> getByEvent(String id);
}



