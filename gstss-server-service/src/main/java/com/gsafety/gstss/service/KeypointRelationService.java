package com.gsafety.gstss.service;

import com.gsafety.gstss.model.misc.KeypointRelationExt;
import com.gsafety.gstss.model.common.ReturnBase;
import com.gsafety.gstss.model.KeypointRelation;

public interface KeypointRelationService  extends BaseService<KeypointRelation>{
    ReturnBase<KeypointRelationExt> getKeyPointRelation(String eventId);
}



