package com.gsafety.gstss.service.impl;

import com.gsafety.gstss.mapper.NewsRegistryMapper;
import com.gsafety.gstss.model.NewsRegistry;
import com.gsafety.gstss.model.common.ReturnBase;
import com.gsafety.gstss.service.NewsRegistryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
@Transactional
public class NewsRegistryServiceImpl extends BaseServiceImpl<NewsRegistry,NewsRegistryMapper> implements NewsRegistryService{

    @Override
    public ReturnBase<List<NewsRegistry>> selectByTime(Date missiondate) {
        NewsRegistryMapper newsRegistryMapper=(NewsRegistryMapper)baseMapper;
        return ReturnBase.succeed(newsRegistryMapper.selectByTime(missiondate));
    }

    @Override
    public ReturnBase insert(List<NewsRegistry> models) {

        for (NewsRegistry model:models) {
            baseMapper.insert(model);
        }
        return ReturnBase.succeed();
    }
}
