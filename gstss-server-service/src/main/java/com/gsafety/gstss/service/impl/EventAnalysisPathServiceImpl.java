package com.gsafety.gstss.service.impl;

import com.gsafety.gstss.model.common.ReturnBase;
import com.gsafety.gstss.mapper.EventAnalysisPathMapper;
import com.gsafety.gstss.model.*;
import com.gsafety.gstss.service.EventAnalysisPathService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(rollbackFor=Exception.class)
public class EventAnalysisPathServiceImpl extends BaseServiceImpl<EventAnalysisPath,EventAnalysisPathMapper> implements EventAnalysisPathService {
    
}




