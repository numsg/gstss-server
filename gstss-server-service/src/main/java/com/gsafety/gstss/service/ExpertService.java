package com.gsafety.gstss.service;

import com.gsafety.gstss.model.common.ReturnBase;
import com.gsafety.gstss.model.Expert;


import java.util.List;

public interface ExpertService  extends BaseService<Expert>{
    ReturnBase<List<Expert>> getByKeyword(String keyword);

    ReturnBase<List<Expert>> getByDomain(String domain);

    ReturnBase<List<Expert>> getByDomainAndKeyword(String domain, String keyword);

    ReturnBase<List<String>> getDomains();
}



