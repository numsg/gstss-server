package com.gsafety.gstss.service;

import com.gsafety.gstss.model.common.ReturnBase;
import com.gsafety.gstss.model.CaseBackground;


import java.util.List;

public interface CaseBackgroundService  extends BaseService<CaseBackground>{
    ReturnBase<List<CaseBackground>> getByCase(String id);
}



