package com.gsafety.gstss.service.impl;

import com.gsafety.gstss.model.common.ReturnBase;
import com.gsafety.gstss.model.*;
import com.gsafety.gstss.mapper.CaseBackgroundMapper;
import com.gsafety.gstss.service.CaseBackgroundService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(rollbackFor=Exception.class)
public class CaseBackgroundServiceImpl extends BaseServiceImpl<CaseBackground,CaseBackgroundMapper> implements CaseBackgroundService {

    @Override
    public ReturnBase<List<CaseBackground>> getByCase(String id) {
        return ReturnBase.succeed(baseMapper.getByCase(id));
    }
}




