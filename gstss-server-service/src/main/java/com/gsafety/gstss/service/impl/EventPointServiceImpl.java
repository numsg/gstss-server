package com.gsafety.gstss.service.impl;

import com.gsafety.gstss.model.common.ReturnBase;
import com.gsafety.gstss.mapper.EventPointMapper;
import com.gsafety.gstss.model.*;
import com.gsafety.gstss.service.EventPointService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(rollbackFor=Exception.class)
public class EventPointServiceImpl extends BaseServiceImpl<EventPoint,EventPointMapper> implements EventPointService {

    @Override
    public List<EventPoint> getKeypointByEvent(String eventId) {
        return baseMapper.getKeypointByEvent(eventId);
    }

    @Override
    public int getSummaryByEvent(String id) {
        return baseMapper.getSummaryByEvent(id);
    }
}




