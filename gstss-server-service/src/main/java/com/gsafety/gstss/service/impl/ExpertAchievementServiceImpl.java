package com.gsafety.gstss.service.impl;

import com.gsafety.gstss.model.common.ReturnBase;
import com.gsafety.gstss.model.*;
import com.gsafety.gstss.mapper.ExpertAchievementMapper;
import com.gsafety.gstss.service.ExpertAchievementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(rollbackFor=Exception.class)
public class ExpertAchievementServiceImpl extends BaseServiceImpl<ExpertAchievement,ExpertAchievementMapper> implements ExpertAchievementService {

    @Override
    public ReturnBase<List<ExpertAchievement>> getByExpert(String expertid) {
        return ReturnBase.succeed(baseMapper.getByExpert(expertid));
    }
}




