package com.gsafety.gstss.service;

import com.gsafety.gstss.model.misc.TreeItem;
import com.gsafety.gstss.model.common.ReturnBase;
import com.gsafety.gstss.model.KeypointItem;


import java.util.List;

public interface KeypointItemService  extends BaseService<KeypointItem>{
    ReturnBase<List<TreeItem>> getByEvent(Integer eventId);
}



