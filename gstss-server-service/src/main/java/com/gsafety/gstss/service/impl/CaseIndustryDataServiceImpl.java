package com.gsafety.gstss.service.impl;

import com.gsafety.gstss.model.common.ReturnBase;
import com.gsafety.gstss.model.*;
import com.gsafety.gstss.mapper.CaseIndustryDataMapper;
import com.gsafety.gstss.model.misc.IndustryDataFilter;
import com.gsafety.gstss.service.CaseIndustryDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional(rollbackFor=Exception.class)
public class CaseIndustryDataServiceImpl extends BaseServiceImpl<CaseIndustryData,CaseIndustryDataMapper> implements CaseIndustryDataService {

    @Override
    public ReturnBase<List<CaseIndustryData>> getByFilter(IndustryDataFilter filter) {
        return ReturnBase.succeed(baseMapper.getByFilter(filter));
    }

    @Override
    public ReturnBase<List<String>> getCategory() {
        return ReturnBase.succeed(baseMapper.getCategory());
    }

    @Override
    public ReturnBase<List<String>> getCategoryByCategoryKeyword(String keyword) {
        return ReturnBase.succeed(baseMapper.getCategoryByCategoryKeyword(keyword));
    }

    @Override
    public ReturnBase<List<String>> getCategoryByThemeKeyword(String keyword) {
        return ReturnBase.succeed(baseMapper.getCategoryByThemeKeyword(keyword));
    }

    @Override
    public ReturnBase<List<String>> getCategoryByIndicatorKeyword(String keyword) {
        return ReturnBase.succeed(baseMapper.getCategoryByIndicatorKeyword(keyword));
    }

    @Override
    public ReturnBase<List<String>> getCategoryByIndustryKeyword(String keyword) {
        return ReturnBase.succeed(baseMapper.getCategoryByIndicatorKeyword(keyword));
    }

    @Override
    public ReturnBase<List<String>> getTheme(String category) {
        List<String> themes=baseMapper.getTheme(category);
        for (int i = 0; i < themes.size(); i++) {
            themes.set(i,themes.get(i).trim());
        }
        return ReturnBase.succeed(themes);
    }

    @Override
    public ReturnBase<List<String>> getThemeByThemeKeyword(String category,String keyword) {
        Map map=new HashMap<>();
        map.put("category",category);
        map.put("keyword",keyword);
        return ReturnBase.succeed(baseMapper.getThemeByThemeKeyword(map));
    }

    @Override
    public ReturnBase<List<String>> getThemeByIndicatorKeyword(String category,String keyword) {
        Map map=new HashMap<>();
        map.put("category",category);
        map.put("keyword",keyword);
        return ReturnBase.succeed(baseMapper.getThemeByIndicatorKeyword(map));
    }

    @Override
    public ReturnBase<List<String>> getThemeByIndustryKeyword(String category,String keyword) {
        Map map=new HashMap<>();
        map.put("category",category);
        map.put("keyword",keyword);
        return ReturnBase.succeed(baseMapper.getThemeByIndustryKeyword(map));
    }

    @Override
    public ReturnBase<List<String>> getIndicator(String category, String theme) {
        Map map=new HashMap<>();
        map.put("category",category);
        map.put("theme",theme);
        return ReturnBase.succeed(baseMapper.getIndicator(map));
    }

    @Override
    public ReturnBase<List<String>> getIndicatorByIndicatorKeyword(String category, String theme,String keyword) {
        Map map=new HashMap<>();
        map.put("category",category);
        map.put("theme",theme);
        map.put("keyword",keyword);
        return ReturnBase.succeed(baseMapper.getIndicatorByIndicatorKeyword(map));
    }

    @Override
    public ReturnBase<List<String>> getIndicatorByIndustryKeyword(String category, String theme,String keyword) {
        Map map=new HashMap<>();
        map.put("category",category);
        map.put("theme",theme);
        map.put("keyword",keyword);
        return ReturnBase.succeed(baseMapper.getIndicatorByIndustryKeyword(map));
    }

    @Override
    public ReturnBase<List<String>> getIndustry(String category, String theme, String indicator) {
        Map map=new HashMap<>();
        map.put("category",category);
        map.put("theme",theme);
        map.put("indicator",indicator);
        return ReturnBase.succeed(baseMapper.getIndustry(map));
    }

    @Override
    public ReturnBase<List<String>> getIndustryByKeyword(String category, String theme, String indicator, String keyword) {
        Map map=new HashMap<>();
        map.put("category",category);
        map.put("theme",theme);
        map.put("indicator",indicator);
        map.put("keyword",keyword);
        return ReturnBase.succeed(baseMapper.getIndustryByKeyword(map));
    }
}




