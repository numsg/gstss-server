package com.gsafety.gstss.service;

import com.gsafety.gstss.model.common.ReturnBase;
import com.gsafety.gstss.model.CasePatent;


import java.util.List;

public interface CasePatentService  extends BaseService<CasePatent>{
    ReturnBase<List<String>> getAllCountry();

    ReturnBase<List<CasePatent>> getByCountry(String country);

    ReturnBase<List<String>> getAllYear();

    ReturnBase<List<CasePatent>> getByYear(String year);
}



