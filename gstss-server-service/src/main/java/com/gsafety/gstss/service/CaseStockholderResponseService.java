package com.gsafety.gstss.service;

import com.gsafety.gstss.model.CasePublicResponse;
import com.gsafety.gstss.model.common.ReturnBase;
import com.gsafety.gstss.model.CaseStockholderResponse;


import java.util.List;

public interface CaseStockholderResponseService  extends BaseService<CaseStockholderResponse>{
    ReturnBase<List<CaseStockholderResponse>> getByCase(String id);
}



