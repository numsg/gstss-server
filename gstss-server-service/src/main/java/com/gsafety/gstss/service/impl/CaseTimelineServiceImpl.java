package com.gsafety.gstss.service.impl;

import com.gsafety.gstss.model.common.ReturnBase;
import com.gsafety.gstss.model.*;
import com.gsafety.gstss.mapper.CaseTimelineMapper;
import com.gsafety.gstss.service.CaseTimelineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(rollbackFor=Exception.class)
public class CaseTimelineServiceImpl extends BaseServiceImpl<CaseTimeline,CaseTimelineMapper> implements CaseTimelineService {
    @Override
    public ReturnBase<List<CaseTimeline>> getByCase(String id) {
        return ReturnBase.succeed(baseMapper.getByCase(id));
    }
}




