package com.gsafety.gstss.service.impl;

import com.gsafety.gstss.model.common.ReturnBase;
import com.gsafety.gstss.model.*;
import com.gsafety.gstss.mapper.CasePatentMapper;
import com.gsafety.gstss.service.CasePatentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(rollbackFor=Exception.class)
public class CasePatentServiceImpl extends BaseServiceImpl<CasePatent,CasePatentMapper> implements CasePatentService {

    @Override
    public ReturnBase<List<String>> getAllCountry() {
        return ReturnBase.succeed(baseMapper.getAllCountry());
    }

    @Override
    public ReturnBase<List<CasePatent>> getByCountry(String country) {
        return ReturnBase.succeed(baseMapper.getByCountry(country));
    }

    @Override
    public ReturnBase<List<String>> getAllYear() {
        return ReturnBase.succeed(baseMapper.getAllYear());
    }

    @Override
    public ReturnBase<List<CasePatent>> getByYear(String year) {
        return ReturnBase.succeed(baseMapper.getByYear(year));
    }
}




