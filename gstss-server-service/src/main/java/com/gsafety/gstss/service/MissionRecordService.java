package com.gsafety.gstss.service;

import com.gsafety.gstss.model.MissionRecord;
import com.gsafety.gstss.model.common.ReturnBase;


import java.util.List;

public interface MissionRecordService  extends BaseService<MissionRecord>{
    ReturnBase insert(List<MissionRecord> models);

    ReturnBase<List<MissionRecord>> GetAllUnfinishedMission();
}
