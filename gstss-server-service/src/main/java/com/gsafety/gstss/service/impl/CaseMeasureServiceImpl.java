package com.gsafety.gstss.service.impl;

import com.gsafety.gstss.model.common.ReturnBase;
import com.gsafety.gstss.model.*;
import com.gsafety.gstss.mapper.CaseMeasureMapper;
import com.gsafety.gstss.service.CaseMeasureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(rollbackFor=Exception.class)
public class CaseMeasureServiceImpl extends BaseServiceImpl<CaseMeasure,CaseMeasureMapper> implements CaseMeasureService {
    @Override
    public ReturnBase<List<CaseMeasure>> getByCase(String id) {
        return ReturnBase.succeed(baseMapper.getByCase(id));
    }
}




