package com.gsafety.gstss.service.impl;

import com.gsafety.gstss.model.common.ReturnBase;
import com.gsafety.gstss.model.*;
import com.gsafety.gstss.mapper.EventRiskMapper;
import com.gsafety.gstss.service.EventRiskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(rollbackFor=Exception.class)
public class EventRiskServiceImpl extends BaseServiceImpl<EventRisk,EventRiskMapper> implements EventRiskService {

    @Override
    public ReturnBase<EventRisk> getByEventId(String id) {
        EventRisk data= baseMapper.getByEventId(id);
        return  ReturnBase.succeed(data);
    }
}




