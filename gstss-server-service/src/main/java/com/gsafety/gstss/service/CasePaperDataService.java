package com.gsafety.gstss.service;

import com.gsafety.gstss.model.common.ReturnBase;
import com.gsafety.gstss.model.CasePaperData;


import java.util.List;

public interface CasePaperDataService  extends BaseService<CasePaperData>{
    ReturnBase<List<String>> getIndicators();

    ReturnBase<List<CasePaperData>> getByIndicator(String indicator);
}



