package com.gsafety.gstss.service;

import com.gsafety.gstss.model.common.ReturnBase;

import java.util.List;
import java.util.Map;

public interface BaseService<T> {
    ReturnBase<T> getByPrimaryKey(String id);

    ReturnBase<List<T>> getAll();

    ReturnBase<List<T>> getByPage(Map map);

    ReturnBase insert(T model);

    ReturnBase insertSelective(T model);

    ReturnBase updateByPrimaryKey(T model);

    ReturnBase updateByPrimaryKeySelective(T model);

    ReturnBase deleteByPrimaryKey(String id);
}
