package com.gsafety.gstss.service.impl;

import com.gsafety.gstss.model.common.ReturnBase;
import com.gsafety.gstss.model.*;
import com.gsafety.gstss.mapper.CaseImageMapper;
import com.gsafety.gstss.service.CaseImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(rollbackFor=Exception.class)
public class CaseImageServiceImpl extends BaseServiceImpl<CaseImage,CaseImageMapper> implements CaseImageService {

    @Override
    public ReturnBase<List<CaseImage>> getByCase(String id) {
        return ReturnBase.succeed(baseMapper.getByCase(id));
    }
}




