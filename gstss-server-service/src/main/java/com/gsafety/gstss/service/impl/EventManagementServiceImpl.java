package com.gsafety.gstss.service.impl;

import com.gsafety.gstss.model.common.ReturnBase;
import com.gsafety.gstss.mapper.EventManagementMapper;
import com.gsafety.gstss.model.*;
import com.gsafety.gstss.service.EventManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(rollbackFor=Exception.class)
public class EventManagementServiceImpl extends BaseServiceImpl<EventManagement,EventManagementMapper> implements EventManagementService {
    
}




