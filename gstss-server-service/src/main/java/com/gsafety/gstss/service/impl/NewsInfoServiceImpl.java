package com.gsafety.gstss.service.impl;

import com.gsafety.gstss.mapper.NewsInfoGroupMapper;
import com.gsafety.gstss.mapper.NewsInfoMapper;
import com.gsafety.gstss.mapper.NewsInfoTagMapper;
import com.gsafety.gstss.model.*;
import com.gsafety.gstss.model.common.ReturnBase;
import com.gsafety.gstss.model.misc.NewsInfoRequeryModel;
import com.gsafety.gstss.model.misc.SummaryItem;
import com.gsafety.gstss.service.MissionRecordService;
import com.gsafety.gstss.service.NewsInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.*;

@Service
@Transactional(rollbackFor=Exception.class)
public class NewsInfoServiceImpl extends BaseServiceImpl<NewsInfo,NewsInfoMapper> implements NewsInfoService {
    @Autowired
    NewsInfoTagMapper newsInfoTagMapper;

    @Autowired
    NewsInfoGroupMapper newsInfoGroupMapper;

    @Autowired
    MissionRecordService missionRecordService;

    @Override
    public ReturnBase insertList(List<NewsInfoExt> models, MissionRecord missionRecord) {

        try{
            for (NewsInfoExt model : models) {
                baseMapper.insert((NewsInfo) model);
                for (NewsInfoGroup group : model.getGroups()) {
                    group.setNewsId(model.getId());
                    newsInfoGroupMapper.insert(group);
                }

                for (NewsInfoTag tag : model.getTags()) {
                    tag.setNewsId(model.getId());
                    newsInfoTagMapper.insert(tag);
                }
            }

            missionRecordService.updateByPrimaryKey(missionRecord);
        }catch (Exception ex)
        {
            System.out.println(ex.getMessage());
        }

        return ReturnBase.succeed();
    }

    @Override
    public ReturnBase<List<NewsInfo>> getByEventItem(Integer itemId) {
        return ReturnBase.succeed(baseMapper.getByKeypoint(itemId));
    }

    @Override
    public ReturnBase<List<SummaryItem>> getSummaryByKeypoint(Integer itemId) {
        List<NewsInfo> newsInfos=baseMapper.getByKeypoint((itemId));
        List<SummaryItem> result=new ArrayList<>();

        if(newsInfos.size()!=0) {
            DateFormat df1 = DateFormat.getDateInstance();
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(newsInfos.get(0).getPubTime());

            calendar.set(Calendar.HOUR_OF_DAY,0);
            calendar.set(Calendar.MINUTE,0);
            calendar.set(Calendar.SECOND,0);

            Date begin=calendar.getTime();
            calendar.add(Calendar.DAY_OF_YEAR,1);
            Date end=calendar.getTime();
            Long count=0L;
            int index=0;

            while(index<newsInfos.size())
            {
                Date date=newsInfos.get(index).getPubTime();
                if(end.compareTo(date)>0)
                {
                    count++;
                    index++;
                }
                else
                {
                    SummaryItem summaryItem=new SummaryItem();
                    summaryItem.setCount(count);

                    summaryItem.setDate(df1.format(begin));
                    result.add(summaryItem);
                    count=0L;
                    begin=end;
                    calendar.add(Calendar.DAY_OF_YEAR,1);
                    end=calendar.getTime();
                }
            }

            SummaryItem summaryItem=new SummaryItem();
            summaryItem.setCount(count);
            summaryItem.setDate(df1.format(begin));
            result.add(summaryItem);
        }
        return  ReturnBase.succeed(result);
    }

    @Override
    public ReturnBase<List<NewsInfo>> getNewsByItemEventDate(NewsInfoRequeryModel model) throws ParseException{
        HashMap map=new HashMap();
        map.put("itemId",model.getEvent());
        DateFormat df=DateFormat.getDateInstance();

        Date date=df.parse(model.getDate());
        map.put("date",date);


        return ReturnBase.succeed(baseMapper.getNewsByItemEventDate(map));
    }

    @Override
    public ReturnBase<List<NewsInfo>> getNewsByKeypointEventDate(NewsInfoRequeryModel model) throws ParseException{
        HashMap map=new HashMap();

        map.put("keypointId",model.getEvent());
        DateFormat df=DateFormat.getDateInstance();

        Date date=df.parse(model.getDate());
        map.put("date",date);
        return ReturnBase.succeed(baseMapper.getNewsByKeypointEventDate(map));
    }

    @Override
    public int getSummaryByEvent(Integer id) {
        return baseMapper.getSummaryByEvent(id);
    }

    @Override
    public ReturnBase<List<SummaryItem>> getSummaryByItem(Integer itemId) {
        List<NewsInfo> newsInfos=baseMapper.getByItem((itemId));
        List<SummaryItem> result=new ArrayList<>();

        if(newsInfos.size()!=0) {
            DateFormat df1 = DateFormat.getDateInstance();
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(newsInfos.get(0).getPubTime());

            calendar.set(Calendar.HOUR_OF_DAY,0);
            calendar.set(Calendar.MINUTE,0);
            calendar.set(Calendar.SECOND,0);

            Date begin=calendar.getTime();
            calendar.add(Calendar.DAY_OF_YEAR,1);
            Date end=calendar.getTime();
            Long count=0L;
            int index=0;

            while(index<newsInfos.size())
            {
                Date date=newsInfos.get(index).getPubTime();
                if(end.compareTo(date)>0)
                {
                    count++;
                    index++;
                }
                else
                {
                    SummaryItem summaryItem=new SummaryItem();
                    summaryItem.setCount(count);
                    summaryItem.setDate(df1.format(begin));
                    result.add(summaryItem);
                    count=0L;
                    begin=end;
                    calendar.add(Calendar.DAY_OF_YEAR,1);
                    end=calendar.getTime();
                }
            }

            SummaryItem summaryItem=new SummaryItem();
            summaryItem.setCount(count);
            summaryItem.setDate(df1.format(begin));
            result.add(summaryItem);
        }
        return  ReturnBase.succeed(result);
    }
}
