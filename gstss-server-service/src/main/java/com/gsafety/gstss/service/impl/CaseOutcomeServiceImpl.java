package com.gsafety.gstss.service.impl;

import com.gsafety.gstss.model.common.ReturnBase;
import com.gsafety.gstss.model.*;
import com.gsafety.gstss.mapper.CaseOutcomeMapper;
import com.gsafety.gstss.service.CaseOutcomeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(rollbackFor=Exception.class)
public class CaseOutcomeServiceImpl extends BaseServiceImpl<CaseOutcome,CaseOutcomeMapper> implements CaseOutcomeService {
    @Override
    public ReturnBase<List<CaseOutcome>> getByCase(String id) {
        return ReturnBase.succeed(baseMapper.getByCase(id));
    }
}




