package com.gsafety.gstss.service.impl;

import com.gsafety.gstss.model.common.ReturnBase;
import com.gsafety.gstss.model.*;
import com.gsafety.gstss.mapper.NewsInfoVideoMapper;
import com.gsafety.gstss.service.NewsInfoVideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(rollbackFor=Exception.class)
public class NewsInfoVideoServiceImpl extends BaseServiceImpl<NewsInfoVideo,NewsInfoVideoMapper> implements NewsInfoVideoService {

    @Override
    public ReturnBase<List<NewsInfoVideo>> getByEvent(String id) {
        return ReturnBase.succeed(baseMapper.getByEvent(id));
    }
}




