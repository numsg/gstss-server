package com.gsafety.gstss.service;

import com.gsafety.gstss.model.common.ReturnBase;
import com.gsafety.gstss.model.CaseMeasure;


import java.util.List;

public interface CaseMeasureService  extends BaseService<CaseMeasure>{
    ReturnBase<List<CaseMeasure>> getByCase(String id);
}



