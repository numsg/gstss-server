package com.gsafety.gstss.service;

import com.gsafety.gstss.model.common.ReturnBase;
import com.gsafety.gstss.model.ExpertAchievement;


import java.util.List;

public interface ExpertAchievementService  extends BaseService<ExpertAchievement>{
    ReturnBase<List<ExpertAchievement>> getByExpert(String expertid);
}



