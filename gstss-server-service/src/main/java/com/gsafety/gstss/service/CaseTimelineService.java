package com.gsafety.gstss.service;

import com.gsafety.gstss.model.common.ReturnBase;
import com.gsafety.gstss.model.CaseTimeline;


import java.util.List;

public interface CaseTimelineService  extends BaseService<CaseTimeline>{
    ReturnBase<List<CaseTimeline>> getByCase(String id);
}



