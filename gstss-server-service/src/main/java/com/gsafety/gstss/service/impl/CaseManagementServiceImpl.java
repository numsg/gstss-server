package com.gsafety.gstss.service.impl;

import com.gsafety.gstss.model.common.ReturnBase;
import com.gsafety.gstss.model.*;
import com.gsafety.gstss.mapper.CaseManagementMapper;
import com.gsafety.gstss.service.CaseManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;

@Service
@Transactional(rollbackFor=Exception.class)
public class CaseManagementServiceImpl extends BaseServiceImpl<CaseManagement,CaseManagementMapper> implements CaseManagementService {

    @Override
    public ReturnBase<List<CaseManagement>> getByTypeName(String typename) {
        return  ReturnBase.succeed(baseMapper.getByTypeName(typename));
    }

    @Override
    public ReturnBase<List<CaseManagement>> getByKeyword(String keyword) {
        return  ReturnBase.succeed(baseMapper.getByKeyword(keyword));
    }

    @Override
    public ReturnBase<List<CaseManagement>> getByKeywordAndType(String keyword, String typename) {
        HashMap<String,String> map=new HashMap<>();
        map.put("industry",typename);
        map.put("keyword",keyword);
        List<CaseManagement> result=baseMapper.getByKeywordAndType(map);
        return  ReturnBase.succeed(result);
    }

    @Override
    public ReturnBase<CaseManagement> getLastCase() {
        List<CaseManagement> result=baseMapper.getLastCase();
        if(result.size()>0) {
            return ReturnBase.succeed(result.get(0));
        }
        else
        {
            return ReturnBase.error(1,"没有查询到结果");
        }
    }
}




