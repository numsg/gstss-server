package com.gsafety.gstss.service.impl;

import com.gsafety.gstss.model.common.ReturnBase;
import com.gsafety.gstss.mapper.BaseMapper;
import com.gsafety.gstss.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;

public abstract class BaseServiceImpl<T,D extends BaseMapper<T>> implements BaseService<T> {

    @Autowired
    protected  D baseMapper;

    @Override
    public ReturnBase<T> getByPrimaryKey(String id) {
        T data= baseMapper.getByPrimaryKey(id);
        return  ReturnBase.succeed(data);
    }
    @Override
    public  ReturnBase<List<T>> getAll() {
        return  ReturnBase.succeed(baseMapper.getAll());
    }
    @Override
    public ReturnBase<List<T>> getByPage(Map map) {
        return ReturnBase.succeed(baseMapper.getByPage(map));
    }
    @Override
    public ReturnBase insert(T model) {
        baseMapper.insert(model);
        return ReturnBase.succeed();
    }
    @Override
    public ReturnBase insertSelective(T model) {
        baseMapper.insertSelective(model);
        return ReturnBase.succeed();
    }
    @Override
    public ReturnBase updateByPrimaryKey(T model) {
        baseMapper.updateByPrimaryKey(model);
        return ReturnBase.succeed();
    }
    @Override
    public ReturnBase updateByPrimaryKeySelective(T model) {
        baseMapper.updateByPrimaryKeySelective(model);
        return ReturnBase.succeed();
    }
    @Override
    public ReturnBase deleteByPrimaryKey(String id) {
        baseMapper.deleteByPrimaryKey(id);
        return ReturnBase.succeed();
    }
}
