package com.gsafety.gstss.service;

import com.gsafety.gstss.model.common.ReturnBase;
import com.gsafety.gstss.model.CaseManagement;


import java.util.List;

public interface CaseManagementService  extends BaseService<CaseManagement>{
    ReturnBase<List<CaseManagement>> getByTypeName(String typename);

    ReturnBase<List<CaseManagement>> getByKeyword(String keyword);

    ReturnBase<List<CaseManagement>> getByKeywordAndType(String keyword, String typename);

    ReturnBase<CaseManagement> getLastCase();
}



