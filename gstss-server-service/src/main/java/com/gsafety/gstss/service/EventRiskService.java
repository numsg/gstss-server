package com.gsafety.gstss.service;

import com.gsafety.gstss.model.common.ReturnBase;
import com.gsafety.gstss.model.EventRisk;


import java.util.List;

public interface EventRiskService  extends BaseService<EventRisk>{
    ReturnBase<EventRisk> getByEventId(String id);
}



