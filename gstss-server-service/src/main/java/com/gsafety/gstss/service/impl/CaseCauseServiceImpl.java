package com.gsafety.gstss.service.impl;

import com.gsafety.gstss.model.common.ReturnBase;
import com.gsafety.gstss.model.*;
import com.gsafety.gstss.mapper.CaseCauseMapper;
import com.gsafety.gstss.service.CaseCauseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(rollbackFor=Exception.class)
public class CaseCauseServiceImpl extends BaseServiceImpl<CaseCause,CaseCauseMapper> implements CaseCauseService {
    @Override
    public ReturnBase<List<CaseCause>> getByCase(String id) {
        return ReturnBase.succeed(baseMapper.getByCase(id));
    }
}




