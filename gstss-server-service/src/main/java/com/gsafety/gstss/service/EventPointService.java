package com.gsafety.gstss.service;

import com.gsafety.gstss.model.common.ReturnBase;
import com.gsafety.gstss.model.EventPoint;


import java.util.List;

public interface EventPointService  extends BaseService<EventPoint>{
    List<EventPoint> getKeypointByEvent(String eventId);

    int getSummaryByEvent(String id);
}



