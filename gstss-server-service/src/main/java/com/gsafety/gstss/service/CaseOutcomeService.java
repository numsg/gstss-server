package com.gsafety.gstss.service;

import com.gsafety.gstss.model.common.ReturnBase;
import com.gsafety.gstss.model.CaseOutcome;


import java.util.List;

public interface CaseOutcomeService  extends BaseService<CaseOutcome>{
    ReturnBase<List<CaseOutcome>> getByCase(String id);
}



