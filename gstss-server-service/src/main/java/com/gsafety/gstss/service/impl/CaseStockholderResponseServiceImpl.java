package com.gsafety.gstss.service.impl;

import com.gsafety.gstss.model.common.ReturnBase;
import com.gsafety.gstss.model.*;
import com.gsafety.gstss.mapper.CaseStockholderResponseMapper;
import com.gsafety.gstss.service.CaseStockholderResponseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(rollbackFor=Exception.class)
public class CaseStockholderResponseServiceImpl extends BaseServiceImpl<CaseStockholderResponse,CaseStockholderResponseMapper> implements CaseStockholderResponseService {

    @Override
    public ReturnBase<List<CaseStockholderResponse>> getByCase(String id) {
        return ReturnBase.succeed(baseMapper.getByCase(id));
    }
}




