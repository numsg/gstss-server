package com.gsafety.gstss.service.impl;

import com.gsafety.gstss.model.common.ReturnBase;
import com.gsafety.gstss.mapper.EventSubcategoryMapper;
import com.gsafety.gstss.model.*;
import com.gsafety.gstss.service.EventSubcategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(rollbackFor=Exception.class)
public class EventSubcategoryServiceImpl extends BaseServiceImpl<EventSubcategory,EventSubcategoryMapper> implements EventSubcategoryService {
    
}




