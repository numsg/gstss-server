package com.gsafety.gstss.service;

import com.gsafety.gstss.model.NewsRegistry;
import com.gsafety.gstss.model.common.ReturnBase;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;


public interface NewsRegistryService extends BaseService<NewsRegistry> {
    ReturnBase<List<NewsRegistry>> selectByTime(Date date);

    ReturnBase insert(List<NewsRegistry> models);
}
