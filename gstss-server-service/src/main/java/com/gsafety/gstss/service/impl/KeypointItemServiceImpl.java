package com.gsafety.gstss.service.impl;

import com.gsafety.gstss.model.common.ReturnBase;
import com.gsafety.gstss.model.*;
import com.gsafety.gstss.mapper.KeypointItemMapper;
import com.gsafety.gstss.model.misc.TreeItem;
import com.gsafety.gstss.service.KeypointItemService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional(rollbackFor=Exception.class)
public class KeypointItemServiceImpl extends BaseServiceImpl<KeypointItem,KeypointItemMapper> implements KeypointItemService {

    @Override
    public ReturnBase<List<TreeItem>> getByEvent(Integer eventId) {
        List<KeypointItem> result=baseMapper.getByEvent(eventId);
        List<TreeItem> items=new ArrayList<>();
        for (KeypointItem keypointItem : result) {
            if(keypointItem.getParent()==null||keypointItem.getParent()==-1)
            {
                TreeItem item=new TreeItem();
                item.setName(keypointItem.getName());
                item.setId(keypointItem.getId());
                item.setValue(2000L);

                items.add(item);
            }
        }
        for (TreeItem item : items) {
            findChild(item,result);
        }
        return  ReturnBase.succeed(items);
    }

    private void findChild(TreeItem item, List<KeypointItem> result) {
        for (KeypointItem keypointItem : result) {
            if(keypointItem.getParent()==item.getId())
            {
                TreeItem childitem=new TreeItem();
                childitem.setName(keypointItem.getName());
                childitem.setId(keypointItem.getId());
                childitem.setValue(keypointItem.getId());

                item.getChildren().add(childitem);
            }
        }

        for (TreeItem treeItem : item.getChildren()) {
            findChild(treeItem,result);
        }
    }
}




