package com.gsafety.gstss.service.impl;

import com.gsafety.gstss.model.common.ReturnBase;
import com.gsafety.gstss.mapper.EventIndustryMapper;
import com.gsafety.gstss.model.*;
import com.gsafety.gstss.service.EventIndustryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(rollbackFor=Exception.class)
public class EventIndustryServiceImpl extends BaseServiceImpl<EventIndustry,EventIndustryMapper> implements EventIndustryService {
    
}




