package com.gsafety.gstss.service.impl;

import com.gsafety.gstss.model.common.ReturnBase;
import com.gsafety.gstss.model.*;
import com.gsafety.gstss.mapper.CaseReferenceMapper;
import com.gsafety.gstss.service.CaseReferenceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(rollbackFor=Exception.class)
public class CaseReferenceServiceImpl extends BaseServiceImpl<CaseReference,CaseReferenceMapper> implements CaseReferenceService {

    @Override
    public ReturnBase<List<String>> getIndicators() {
        return  ReturnBase.succeed(baseMapper.getIndicators());
    }

    @Override
    public ReturnBase<List<CaseReference>> getByIndicator(String indicator) {
        return  ReturnBase.succeed(baseMapper.getByIndicator(indicator));
    }
}




