package com.gsafety.gstss.service;

import com.gsafety.gstss.model.MissionRecord;
import com.gsafety.gstss.model.NewsInfo;
import com.gsafety.gstss.model.NewsInfoExt;
import com.gsafety.gstss.model.misc.NewsInfoRequeryModel;
import com.gsafety.gstss.model.misc.SummaryItem;
import com.gsafety.gstss.model.common.ReturnBase;

import java.text.ParseException;
import java.util.List;

public interface NewsInfoService extends BaseService<NewsInfo> {
    ReturnBase insertList(List<NewsInfoExt> models, MissionRecord missionRecord);

    ReturnBase<List<NewsInfo>> getByEventItem(Integer itemId);

    ReturnBase<List<SummaryItem>> getSummaryByKeypoint(Integer itemId);

    ReturnBase<List<NewsInfo>> getNewsByItemEventDate(NewsInfoRequeryModel model) throws ParseException;

    ReturnBase<List<NewsInfo>> getNewsByKeypointEventDate(NewsInfoRequeryModel model) throws ParseException;

    int getSummaryByEvent(Integer id);

    ReturnBase<List<SummaryItem>> getSummaryByItem(Integer itemId);
}
