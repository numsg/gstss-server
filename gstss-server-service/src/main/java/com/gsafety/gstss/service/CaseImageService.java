package com.gsafety.gstss.service;

import com.gsafety.gstss.model.common.ReturnBase;
import com.gsafety.gstss.model.CaseImage;


import java.util.List;

public interface CaseImageService  extends BaseService<CaseImage>{
    ReturnBase<List<CaseImage>> getByCase(String id);
}



