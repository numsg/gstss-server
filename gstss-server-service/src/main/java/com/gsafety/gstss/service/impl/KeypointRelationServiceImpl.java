package com.gsafety.gstss.service.impl;

import com.gsafety.gstss.model.common.ReturnBase;
import com.gsafety.gstss.model.*;
import com.gsafety.gstss.mapper.KeypointRelationMapper;
import com.gsafety.gstss.model.misc.KeypointRelationExt;
import com.gsafety.gstss.service.EventPointService;
import com.gsafety.gstss.service.KeypointRelationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(rollbackFor=Exception.class)
public class KeypointRelationServiceImpl extends BaseServiceImpl<KeypointRelation,KeypointRelationMapper> implements KeypointRelationService {

    @Autowired
    EventPointService eventPointService;

    @Override
    public ReturnBase<KeypointRelationExt> getKeyPointRelation(String eventId) {
        KeypointRelationExt keypointRelationExt=new KeypointRelationExt();
        keypointRelationExt.setKeypoints(eventPointService.getKeypointByEvent(eventId));
        keypointRelationExt.setRelations(baseMapper.getEventRelation(eventId));
        return ReturnBase.succeed(keypointRelationExt);
    }
}




