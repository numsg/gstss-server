package com.gsafety.gstss.mapper;

import com.gsafety.gstss.model.KeypointRelation;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface KeypointRelationMapper extends BaseMapper<KeypointRelation>{
    List<KeypointRelation> getEventRelation(String eventId);
}


