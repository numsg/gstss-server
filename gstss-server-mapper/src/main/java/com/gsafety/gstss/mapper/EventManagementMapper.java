package com.gsafety.gstss.mapper;


import com.gsafety.gstss.model.EventManagement;
import org.springframework.stereotype.Service;

@Service
public interface EventManagementMapper extends BaseMapper<EventManagement>{

}


