package com.gsafety.gstss.mapper;


import com.gsafety.gstss.model.EventAnalysisDomain;
import org.springframework.stereotype.Service;

@Service
public interface EventAnalysisDomainMapper extends BaseMapper<EventAnalysisDomain>{

}


