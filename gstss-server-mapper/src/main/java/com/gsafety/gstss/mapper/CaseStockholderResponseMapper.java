package com.gsafety.gstss.mapper;

import com.gsafety.gstss.model.CaseStockholderResponse;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CaseStockholderResponseMapper extends BaseMapper<CaseStockholderResponse>{

    List<CaseStockholderResponse> getByCase(String id);
}


