package com.gsafety.gstss.mapper;

import com.gsafety.gstss.model.NewsInfoGroup;
import org.springframework.stereotype.Service;

@Service
public interface NewsInfoGroupMapper extends BaseMapper<NewsInfoGroup>{

}