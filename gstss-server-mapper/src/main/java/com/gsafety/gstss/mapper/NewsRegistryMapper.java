package com.gsafety.gstss.mapper;

import com.gsafety.gstss.model.NewsRegistry;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
@Service
public interface NewsRegistryMapper extends BaseMapper<NewsRegistry>{
    List<NewsRegistry> selectByTime(Date date);
}