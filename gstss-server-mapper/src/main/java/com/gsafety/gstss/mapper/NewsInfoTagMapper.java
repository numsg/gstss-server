package com.gsafety.gstss.mapper;

import com.gsafety.gstss.model.NewsInfoTag;
import org.springframework.stereotype.Service;

@Service
public interface NewsInfoTagMapper extends BaseMapper<NewsInfoTag>{

}