package com.gsafety.gstss.mapper;

import com.gsafety.gstss.model.NewsInfoVideo;
import com.gsafety.gstss.model.common.ReturnBase;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface NewsInfoVideoMapper extends BaseMapper<NewsInfoVideo>{
    List<NewsInfoVideo> getByEvent(String eventId);
}


