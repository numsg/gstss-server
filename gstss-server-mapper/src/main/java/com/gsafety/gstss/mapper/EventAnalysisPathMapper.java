package com.gsafety.gstss.mapper;


import com.gsafety.gstss.model.EventAnalysisPath;
import org.springframework.stereotype.Service;

@Service
public interface EventAnalysisPathMapper extends BaseMapper<EventAnalysisPath>{

}


