package com.gsafety.gstss.mapper;


import com.gsafety.gstss.model.AnalysisPath;
import org.springframework.stereotype.Service;

@Service
public interface AnalysisPathMapper extends BaseMapper<AnalysisPath>{

}


