package com.gsafety.gstss.mapper;

import com.gsafety.gstss.model.CaseBackground;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CaseBackgroundMapper extends BaseMapper<CaseBackground>{
    List<CaseBackground> getByCase(String id);
}


