package com.gsafety.gstss.mapper;

import com.gsafety.gstss.model.Expert;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public interface ExpertMapper extends BaseMapper<Expert>{
    List<Expert> getByKeyword(String id);

    List<Expert> getByDomain(String domain);

    List<Expert> getByDomainAndKeyword(Map map);

    List<String> getDomains();
}


