package com.gsafety.gstss.mapper;

import com.gsafety.gstss.model.CasePublicResponse;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CasePublicResponseMapper extends BaseMapper<CasePublicResponse>{

    List<CasePublicResponse> getByCase(String id);
}


