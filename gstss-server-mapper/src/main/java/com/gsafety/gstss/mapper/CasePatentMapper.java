package com.gsafety.gstss.mapper;

import com.gsafety.gstss.model.CasePatent;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CasePatentMapper extends BaseMapper<CasePatent>{

    List<String> getAllCountry();

    List<CasePatent> getByCountry(String country);

    List<String> getAllYear();

    List<CasePatent> getByYear(String year);
}


