package com.gsafety.gstss.mapper;

import com.gsafety.gstss.model.EventIndustry;
import org.springframework.stereotype.Service;

@Service
public interface EventIndustryMapper extends BaseMapper<EventIndustry>{

}


