package com.gsafety.gstss.mapper;

import com.gsafety.gstss.model.NewsInfo;
import com.gsafety.gstss.model.misc.NewsInfoRequeryModel;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

@Service
public interface NewsInfoMapper extends BaseMapper<NewsInfo>{
    List<NewsInfo> getByKeypoint(Integer itemId);

    List<NewsInfo> getByItem(Integer itemId);

    List<NewsInfo> getNewsByItemEventDate(HashMap map);

    List<NewsInfo> getNewsByKeypointEventDate(HashMap map);

    int getSummaryByEvent(Integer id);
}