package com.gsafety.gstss.mapper;

import com.gsafety.gstss.model.KeypointAdvice;
import org.springframework.stereotype.Service;

@Service
public interface KeypointAdviceMapper extends BaseMapper<KeypointAdvice>{

}


