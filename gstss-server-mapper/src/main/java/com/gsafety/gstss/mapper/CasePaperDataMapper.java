package com.gsafety.gstss.mapper;

import com.gsafety.gstss.model.CasePaperData;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CasePaperDataMapper extends BaseMapper<CasePaperData>{

    List<String> getIndicators();

    List<CasePaperData> getByIndicator(String indicator);
}


