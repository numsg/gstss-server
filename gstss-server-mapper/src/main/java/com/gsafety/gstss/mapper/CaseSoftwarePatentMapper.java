package com.gsafety.gstss.mapper;

import com.gsafety.gstss.model.CaseSoftwarePatent;
import org.springframework.stereotype.Service;

@Service
public interface CaseSoftwarePatentMapper extends BaseMapper<CaseSoftwarePatent>{

}


