package com.gsafety.gstss.mapper;

import com.gsafety.gstss.model.EventRisk;
import org.springframework.stereotype.Service;

@Service
public interface EventRiskMapper extends BaseMapper<EventRisk>{
    EventRisk getByEventId(String id);
}


