package com.gsafety.gstss.mapper;

import com.gsafety.gstss.model.CaseOutcome;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CaseOutcomeMapper extends BaseMapper<CaseOutcome>{

    List<CaseOutcome> getByCase(String id);
}


