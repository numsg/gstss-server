package com.gsafety.gstss.mapper;


import com.gsafety.gstss.model.EventSubcategory;
import org.springframework.stereotype.Service;

@Service
public interface EventSubcategoryMapper extends BaseMapper<EventSubcategory>{

}


