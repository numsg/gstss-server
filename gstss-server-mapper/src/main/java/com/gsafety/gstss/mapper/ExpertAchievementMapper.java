package com.gsafety.gstss.mapper;

import com.gsafety.gstss.model.ExpertAchievement;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ExpertAchievementMapper extends BaseMapper<ExpertAchievement>{

    List<ExpertAchievement> getByExpert(String expertid);
}


