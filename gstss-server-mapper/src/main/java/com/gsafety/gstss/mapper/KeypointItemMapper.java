package com.gsafety.gstss.mapper;

import com.gsafety.gstss.model.KeypointItem;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface KeypointItemMapper extends BaseMapper<KeypointItem>{

    List<KeypointItem> getByEvent(Integer eventId);
}


