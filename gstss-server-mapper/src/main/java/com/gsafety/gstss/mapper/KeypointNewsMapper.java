package com.gsafety.gstss.mapper;


import com.gsafety.gstss.model.KeypointNews;
import org.springframework.stereotype.Service;

@Service
public interface KeypointNewsMapper extends BaseMapper<KeypointNews>{

}


