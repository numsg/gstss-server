package com.gsafety.gstss.mapper;

import com.gsafety.gstss.model.CaseReference;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CaseReferenceMapper extends BaseMapper<CaseReference>{
    List<String> getIndicators();

    List<CaseReference> getByIndicator(String indicator);
}


