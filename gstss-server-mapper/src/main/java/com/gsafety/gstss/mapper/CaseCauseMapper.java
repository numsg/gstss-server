package com.gsafety.gstss.mapper;

import com.gsafety.gstss.model.CaseCause;
import org.apache.ibatis.annotations.Case;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CaseCauseMapper extends BaseMapper<CaseCause>{
    List<CaseCause> getByCase(String id);
}


