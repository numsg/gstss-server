package com.gsafety.gstss.mapper;

import com.gsafety.gstss.model.CaseTimeline;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CaseTimelineMapper extends BaseMapper<CaseTimeline>{

    List<CaseTimeline> getByCase(String id);
}


