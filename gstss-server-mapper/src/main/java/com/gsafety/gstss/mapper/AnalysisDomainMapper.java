package com.gsafety.gstss.mapper;

import com.gsafety.gstss.model.AnalysisDomain;
import org.springframework.stereotype.Service;

@Service
public interface AnalysisDomainMapper extends BaseMapper<AnalysisDomain>{

}


