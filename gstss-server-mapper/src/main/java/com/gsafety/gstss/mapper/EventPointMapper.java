package com.gsafety.gstss.mapper;

import com.gsafety.gstss.model.EventPoint;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface EventPointMapper extends BaseMapper<EventPoint>{
    List<EventPoint> getKeypointByEvent(String eventId);

    int getSummaryByEvent(String id);
}


