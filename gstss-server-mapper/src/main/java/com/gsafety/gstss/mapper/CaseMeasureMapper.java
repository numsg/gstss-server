package com.gsafety.gstss.mapper;

import com.gsafety.gstss.model.CaseMeasure;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CaseMeasureMapper extends BaseMapper<CaseMeasure>{

    List<CaseMeasure> getByCase(String id);
}


