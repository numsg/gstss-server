package com.gsafety.gstss.mapper;


import com.gsafety.gstss.model.EventCategory;
import org.springframework.stereotype.Service;

@Service
public interface EventCategoryMapper extends BaseMapper<EventCategory>{

}


