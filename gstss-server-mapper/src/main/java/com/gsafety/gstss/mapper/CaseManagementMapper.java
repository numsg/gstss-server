package com.gsafety.gstss.mapper;

import com.gsafety.gstss.model.CaseManagement;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public interface CaseManagementMapper extends BaseMapper<CaseManagement>{

    List<CaseManagement> getByTypeName(String typename);

    List<CaseManagement> getByKeyword(String keyword);

    List<CaseManagement> getByKeywordAndType(Map map);

    List<CaseManagement> getLastCase();
}


