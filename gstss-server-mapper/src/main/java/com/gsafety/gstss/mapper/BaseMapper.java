package com.gsafety.gstss.mapper;


import java.util.List;
import java.util.Map;

public interface BaseMapper<T> {
    T getByPrimaryKey(String id);

    List<T> getAll();

    List<T> getByPage(Map map);

    int insert(T model);

    int insertSelective(T model);

    int updateByPrimaryKey(T model);

    int updateByPrimaryKeySelective(T model);

    int deleteByPrimaryKey(String id);
}
