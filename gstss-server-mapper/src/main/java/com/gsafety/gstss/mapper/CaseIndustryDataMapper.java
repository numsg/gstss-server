package com.gsafety.gstss.mapper;

import com.gsafety.gstss.model.CaseIndustryData;
import com.gsafety.gstss.model.misc.IndustryDataFilter;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public interface CaseIndustryDataMapper extends BaseMapper<CaseIndustryData>{

    List<CaseIndustryData> getByFilter(IndustryDataFilter filter);

    List<String> getCategory();

    List<String> getCategoryByCategoryKeyword(String keyword);

    List<String> getCategoryByThemeKeyword(String keyword);

    List<String> getCategoryByIndicatorKeyword(String keyword);

    List<String> getCategoryByIndustryKeyword(String keyword);

    List<String> getTheme(String category);

    List<String> getThemeByThemeKeyword(Map map);

    List<String> getThemeByIndicatorKeyword(Map map);

    List<String> getThemeByIndustryKeyword(Map map);

    List<String> getIndicator(Map map);

    List<String> getIndicatorByIndicatorKeyword(Map map);

    List<String> getIndicatorByIndustryKeyword(Map map);

    List<String> getIndustry(Map map);

    List<String> getIndustryByKeyword(Map map);
}


