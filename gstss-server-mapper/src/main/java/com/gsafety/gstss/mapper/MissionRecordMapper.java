package com.gsafety.gstss.mapper;

import com.gsafety.gstss.model.MissionRecord;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface MissionRecordMapper extends BaseMapper<MissionRecord>{
    List<MissionRecord> GetAllUnfinishedMission();
}