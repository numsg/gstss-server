package com.gsafety.gstss.mapper;

import com.gsafety.gstss.model.CaseImage;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CaseImageMapper extends BaseMapper<CaseImage>{
    List<CaseImage> getByCase(String id);
}


