package com.gsafety.gstss.model.misc;

import java.util.ArrayList;
import java.util.List;

public class TreeItem {

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    Long id;

    public TreeItem()
    {
        children=new ArrayList<>();
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setValue(Long value) {
        this.value = value;
    }

    String name;

    public String getName() {
        return name;
    }

    public Long getValue() {
        return value;
    }

    Long value;

    public List<TreeItem> getChildren() {
        return children;
    }

    public void setChildren(List<TreeItem> children) {
        this.children = children;
    }

    List<TreeItem> children;
}
