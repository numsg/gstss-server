package com.gsafety.gstss.model;

import java.util.List;

public class NewsInfoExt extends NewsInfo {
    public void setGroups(List<NewsInfoGroup> groups) {
        this.groups = groups;
    }

    public void setTags(List<NewsInfoTag> tags) {
        this.tags = tags;
    }

    List<NewsInfoGroup> groups;

    public List<NewsInfoGroup> getGroups() {
        return groups;
    }

    public List<NewsInfoTag> getTags() {
        return tags;
    }

    List<NewsInfoTag> tags;
}
