package com.gsafety.gstss.model;

import java.io.Serializable;

/**
 * 
 * @author 
 */
public class EventAnalysisPath implements Serializable {

	private static final long serialVersionUID = 1L;
	/**
	* 
	*/
	private Long id;
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id= id;
    }

	/**
	* 分析路径
	*/
	private Long analysisPath;
    public Long getAnalysisPath() {
        return analysisPath;
    }

    public void setAnalysisPath(Long analysisPath) {
        this.analysisPath= analysisPath;
    }

	/**
	* 事件
	*/
	private String eventId;
    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId= eventId;
    }


}

