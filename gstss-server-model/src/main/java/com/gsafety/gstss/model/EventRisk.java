package com.gsafety.gstss.model;

import java.io.Serializable;

/**
 * 
 * @author 
 */
public class EventRisk implements Serializable {

	private static final long serialVersionUID = 1L;
	/**
	* 
	*/
	private Long id;
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id= id;
    }

	/**
	* 事件
	*/
	private Long eventId;
    public Long getEventId() {
        return eventId;
    }

    public void setEventId(Long eventId) {
        this.eventId= eventId;
    }

	/**
	* 科技人才
	*/
	private Float humanResouce;
    public Float getHumanResouce() {
        return humanResouce;
    }

    public void setHumanResouce(Float humanResouce) {
        this.humanResouce= humanResouce;
    }

	/**
	* 国际贸易
	*/
	private Float internationalTrade;
    public Float getInternationalTrade() {
        return internationalTrade;
    }

    public void setInternationalTrade(Float internationalTrade) {
        this.internationalTrade= internationalTrade;
    }

	/**
	* 国内市场
	*/
	private Float civilMarket;
    public Float getCivilMarket() {
        return civilMarket;
    }

    public void setCivilMarket(Float civilMarket) {
        this.civilMarket= civilMarket;
    }

	/**
	* 科技发展
	*/
	private Float technologyDevelopment;
    public Float getTechnologyDevelopment() {
        return technologyDevelopment;
    }

    public void setTechnologyDevelopment(Float technologyDevelopment) {
        this.technologyDevelopment= technologyDevelopment;
    }

	/**
	* 知识转化
	*/
	private Float knowledgeCommercial;
    public Float getKnowledgeCommercial() {
        return knowledgeCommercial;
    }

    public void setKnowledgeCommercial(Float knowledgeCommercial) {
        this.knowledgeCommercial= knowledgeCommercial;
    }

	/**
	* 
	*/
	private Float riskValue;
    public Float getRiskValue() {
        return riskValue;
    }

    public void setRiskValue(Float riskValue) {
        this.riskValue= riskValue;
    }


}

