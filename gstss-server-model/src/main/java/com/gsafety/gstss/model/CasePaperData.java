package com.gsafety.gstss.model;

import java.io.Serializable;

/**
 * 
 * @author 
 */
public class CasePaperData implements Serializable {

	private static final long serialVersionUID = 1L;
	/**
	* 
	*/
	private Long id;
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id= id;
    }

	/**
	* 鍚嶇О
	*/
	private String category;
    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category= category;
    }

	/**
	* 
	*/
	private String theme;
    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme= theme;
    }

	/**
	* 
	*/
	private String indicator;
    public String getIndicator() {
        return indicator;
    }

    public void setIndicator(String indicator) {
        this.indicator= indicator;
    }

	/**
	* 
	*/
	private String year;
    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year= year;
    }

	/**
	* 
	*/
	private Float amount;
    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount= amount;
    }

	/**
	* 
	*/
	private String unit;
    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit= unit;
    }

	/**
	* 
	*/
	private String industry;
    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry= industry;
    }


}

