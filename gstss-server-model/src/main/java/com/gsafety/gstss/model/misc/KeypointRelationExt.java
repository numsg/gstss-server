package com.gsafety.gstss.model.misc;

import com.gsafety.gstss.model.EventPoint;
import com.gsafety.gstss.model.KeypointRelation;

import java.util.List;

public class KeypointRelationExt {

    public void setKeypoints(List<EventPoint> keypoints) {
        this.keypoints = keypoints;
    }

    public void setRelations(List<KeypointRelation> relations) {
        this.relations = relations;
    }

    public List<EventPoint> getKeypoints() {
        return keypoints;
    }

    List<EventPoint> keypoints;

    public List<KeypointRelation> getRelations() {
        return relations;
    }

    List<KeypointRelation> relations;
}
