package com.gsafety.gstss.model;

import java.io.Serializable;

/**
 * 
 * @author 
 */
public class CaseReference implements Serializable {

	private static final long serialVersionUID = 1L;
	/**
	* 
	*/
	private Long id;
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id= id;
    }

	/**
	* 
	*/
	private String indicator;
    public String getIndicator() {
        return indicator;
    }

    public void setIndicator(String indicator) {
        this.indicator= indicator;
    }

	/**
	* 
	*/
	private String year;
    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year= year;
    }

	/**
	* 
	*/
	private Float amount;
    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount= amount;
    }


}

