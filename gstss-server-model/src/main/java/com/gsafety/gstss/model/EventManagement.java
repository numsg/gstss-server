package com.gsafety.gstss.model;

import java.io.Serializable;

/**
 * 
 * @author 
 */
public class EventManagement implements Serializable {

	private static final long serialVersionUID = 1L;
	/**
	* 
	*/
	private Long id;
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id= id;
    }

	/**
	* 事件名称
	*/
	private String eventName;
    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName= eventName;
    }

	/**
	* 事件类型
	*/
	private Long eventCategory;
    public Long getEventCategory() {
        return eventCategory;
    }

    public void setEventCategory(Long eventCategory) {
        this.eventCategory= eventCategory;
    }

	/**
	* 事件子类型
	*/
	private Long eventSubcategory;
    public Long getEventSubcategory() {
        return eventSubcategory;
    }

    public void setEventSubcategory(Long eventSubcategory) {
        this.eventSubcategory= eventSubcategory;
    }

	/**
	* 事件主体
	*/
	private String country;
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country= country;
    }

	/**
	* 行业
	*/
	private Long industry;
    public Long getIndustry() {
        return industry;
    }

    public void setIndustry(Long industry) {
        this.industry= industry;
    }

	/**
	* 事件描述
	*/
	private String description;
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description= description;
    }

	/**
	* 分析路径
	*/
	private Long analysisPath;
    public Long getAnalysisPath() {
        return analysisPath;
    }

    public void setAnalysisPath(Long analysisPath) {
        this.analysisPath= analysisPath;
    }

	/**
	* 分析领域
	*/
	private Long analysisDomain;
    public Long getAnalysisDomain() {
        return analysisDomain;
    }

    public void setAnalysisDomain(Long analysisDomain) {
        this.analysisDomain= analysisDomain;
    }

	/**
	* 关键词
	*/
	private String keyword;
    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword= keyword;
    }

	/**
	* 
	*/
	private Byte monitoring;
    public Byte getMonitoring() {
        return monitoring;
    }

    public void setMonitoring(Byte monitoring) {
        this.monitoring= monitoring;
    }


}

