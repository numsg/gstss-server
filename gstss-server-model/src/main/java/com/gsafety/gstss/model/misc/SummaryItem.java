package com.gsafety.gstss.model.misc;

import java.io.Serializable;
import java.util.Date;

public class SummaryItem implements Serializable {
    private static final long serialVersionUID = 1L;

    public String getDate() {
        return date;
    }

    public Long getCount() {
        return count;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    String date;

    Long count;
}
