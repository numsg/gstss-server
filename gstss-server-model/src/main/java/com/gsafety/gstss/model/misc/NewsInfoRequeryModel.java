package com.gsafety.gstss.model.misc;

import java.util.Date;

public class NewsInfoRequeryModel {
    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    String date;

    public String getEvent() {
        return event;
    }

    String event;
}
