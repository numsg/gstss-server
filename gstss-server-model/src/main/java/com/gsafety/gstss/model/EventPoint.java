package com.gsafety.gstss.model;

import java.io.Serializable;

/**
 * 
 * @author 
 */
public class EventPoint implements Serializable {

	private static final long serialVersionUID = 1L;
	/**
	* 
	*/
	private Long id;
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id= id;
    }

	/**
	* 分析点
	*/
	private String keypoint;
    public String getKeypoint() {
        return keypoint;
    }

    public void setKeypoint(String keypoint) {
        this.keypoint= keypoint;
    }

	/**
	* 
	*/
	private String eventId;
    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId= eventId;
    }

	/**
	* 
	*/
	private Long parent;
    public Long getParent() {
        return parent;
    }

    public void setParent(Long parent) {
        this.parent= parent;
    }

	/**
	* 数量
	*/
	private Long count;
    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count= count;
    }


}

