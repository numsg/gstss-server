package com.gsafety.gstss.model;

import java.io.Serializable;

/**
 * 
 * @author 
 */
public class CasePatent implements Serializable {

	private static final long serialVersionUID = 1L;
	/**
	* 
	*/
	private Long id;
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id= id;
    }

	/**
	* 閸氬秶袨
	*/
	private String country;
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country= country;
    }

	/**
	* 
	*/
	private String year;
    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year= year;
    }

	/**
	* 
	*/
	private Float amount;
    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount= amount;
    }


}

