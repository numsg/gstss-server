package com.gsafety.gstss.model;

import java.io.Serializable;

/**
 * 
 * @author 
 */
public class ExpertAchievement implements Serializable {

	private static final long serialVersionUID = 1L;
	/**
	* 
	*/
	private Long id;
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id= id;
    }

	/**
	* 閸氬秶袨
	*/
	private Long expert;
    public Long getExpert() {
        return expert;
    }

    public void setExpert(Long expert) {
        this.expert= expert;
    }

	/**
	* 鍗曚綅
	*/
	private String name;
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name= name;
    }

	/**
	* 
	*/
	private String path;
    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path= path;
    }


}

