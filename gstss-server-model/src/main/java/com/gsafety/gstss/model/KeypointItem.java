package com.gsafety.gstss.model;

import java.io.Serializable;

/**
 * 
 * @author 
 */
public class KeypointItem implements Serializable {

	private static final long serialVersionUID = 1L;
	/**
	* 
	*/
	private Long id;
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id= id;
    }

	/**
	* 名称
	*/
	private String name;
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name= name;
    }

	/**
	* 
	*/
	private float ratio;
    public float getRatio() {
        return ratio;
    }

    public void setRatio(float ratio) {
        this.ratio= ratio;
    }

	/**
	* 
	*/
	private Long keypoint;
    public Long getKeypoint() {
        return keypoint;
    }

    public void setKeypoint(Long keypoint) {
        this.keypoint= keypoint;
    }

	/**
	* 
	*/
	private Long parent;
    public Long getParent() {
        return parent;
    }

    public void setParent(Long parent) {
        this.parent= parent;
    }


}

