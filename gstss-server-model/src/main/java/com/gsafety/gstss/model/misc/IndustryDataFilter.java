package com.gsafety.gstss.model.misc;

public class IndustryDataFilter {
    String category;

    public String getCategory() {
        return category;
    }

    public String getTheme() {
        return theme;
    }

    public String getIndicator() {
        return indicator;
    }

    public String getIndustry() {
        return industry;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public void setIndicator(String indicator) {
        this.indicator = indicator;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    String theme;

    String indicator;

    String industry;
}
