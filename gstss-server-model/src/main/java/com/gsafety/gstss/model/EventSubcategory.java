package com.gsafety.gstss.model;

import java.io.Serializable;

/**
 * 
 * @author 
 */
public class EventSubcategory implements Serializable {

	private static final long serialVersionUID = 1L;
	/**
	* 
	*/
	private Long id;
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id= id;
    }

	/**
	* 名称
	*/
	private String name;
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name= name;
    }

	/**
	* 事件类型
	*/
	private Long category;
    public Long getCategory() {
        return category;
    }

    public void setCategory(Long category) {
        this.category= category;
    }


}

