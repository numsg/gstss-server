package com.gsafety.gstss.model.common;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Locale;

import org.springframework.context.NoSuchMessageException;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.context.MessageSource;

public class ReturnBase<T> {

    private Error error;

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    private boolean success;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccees(boolean succeess) {
        this.success = succeess;
    }

    private T data;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public long getDataCount() {
        return dataCount;
    }

    private long dataCount;

    public void setDataCount(long dataCount) {
        this.dataCount = dataCount;
    }

    private static Logger logger = LoggerFactory.getLogger(ReturnBase.class);


    public static ReturnBase succeed() {
        ReturnBase returnBase = new ReturnBase();
        returnBase.setSuccees(true);
        return returnBase;
    }

    public static <W> ReturnBase<W> succeed(W w) {
        ReturnBase<W> returnBase = new ReturnBase<W>();
        returnBase.setData(w);
        returnBase.setSuccees(true);

        return returnBase;
    }

    public static <N> ReturnBase<List<N>> succeed(List<N> data, long dataCount) {
        ReturnBase<List<N>> returnBase = new ReturnBase<List<N>>();
        returnBase.setData(data);
        returnBase.setDataCount(dataCount);
        returnBase.setSuccees(true);

        return returnBase;
    }

    public static ReturnBase error(int errorCode,MessageSource messageSource,Object ...args){
        Error error;
        if(messageSource==null){
            error=new Error(errorCode,getErrorMessage(errorCode,args));
        }
        else{
            String key="errorCode:"+errorCode;
            String message=getErrorMessage(errorCode,key,args);
            error=new Error(errorCode,message);
        }

        return buildError((error));
    }

    private static ReturnBase buildError(Error error) {
        ReturnBase returnBase = new ReturnBase();
        returnBase.setError(error);
        returnBase.setSuccees(false);

        return returnBase;
    }

    public static ReturnBase error(int errorCode, String message) {
        Error error = new Error(errorCode, message);
        return buildError(error);
    }

    private static String getErrorMessage(int errorCode, Object... args) {
        String key = "errorCode." + errorCode;
        ResourceBundleMessageSource resourceBundleMessageSource = new ResourceBundleMessageSource();
        resourceBundleMessageSource.setBasename("message");
        resourceBundleMessageSource.setDefaultEncoding("UTF-8");
        return getKeyMessage(resourceBundleMessageSource, key, args);
    }

    private static String getKeyMessage(MessageSource messageSource, String key, Object... args) {
        try {
            return messageSource.getMessage(key, args, Locale.getDefault());
        } catch (NoSuchMessageException ex) {
            logger.error(ex.getMessage(), ex);
        }

        return "";
    }

}
