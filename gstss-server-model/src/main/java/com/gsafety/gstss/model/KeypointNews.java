package com.gsafety.gstss.model;

import java.io.Serializable;

/**
 * 
 * @author 
 */
public class KeypointNews implements Serializable {

	private static final long serialVersionUID = 1L;
	/**
	* 
	*/
	private Long id;
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id= id;
    }

	/**
	* 分析点
	*/
	private Long keypointItem;
    public Long getKeypointItem() {
        return keypointItem;
    }

    public void setKeypointItem(Long keypointItem) {
        this.keypointItem= keypointItem;
    }

	/**
	* 事件
	*/
	private String newsinfoId;
    public String getNewsinfoId() {
        return newsinfoId;
    }

    public void setNewsinfoId(String newsinfoId) {
        this.newsinfoId= newsinfoId;
    }


}

