package com.gsafety.gstss.model;

import java.io.Serializable;

/**
 * 
 * @author 
 */
public class Expert implements Serializable {

	private static final long serialVersionUID = 1L;
	/**
	* 
	*/
	private Long id;
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id= id;
    }

	/**
	* 鍚嶇О
	*/
	private String name;
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name= name;
    }

	/**
	* 单位
	*/
	private String enterprise;
    public String getEnterprise() {
        return enterprise;
    }

    public void setEnterprise(String enterprise) {
        this.enterprise= enterprise;
    }

	/**
	* 
	*/
	private String special;
    public String getSpecial() {
        return special;
    }

    public void setSpecial(String special) {
        this.special= special;
    }

	/**
	* 
	*/
	private String email;
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email= email;
    }

	/**
	* 
	*/
	private String page;
    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page= page;
    }

	/**
	* 
	*/
	private String memo;
    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo= memo;
    }


}

