package com.gsafety.gstss.model;

import java.io.Serializable;

/**
 * 
 * @author 
 */
public class CaseManagement implements Serializable {

	private static final long serialVersionUID = 1L;
	/**
	* 
	*/
	private Long id;
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id= id;
    }

	/**
	/**
	* 名称
	*/
	private String caseName;
    public String getCaseName() {
        return caseName;
    }

    public void setCaseName(String caseName) {
        this.caseName= caseName;
    }

	/**
	* 发生时间
	*/
	private String caseTime;
    public String getCaseTime() {
        return caseTime;
    }

    public void setCaseTime(String caseTime) {
        this.caseTime= caseTime;
    }

	/**
	* 事件分类
	*/
	private String caseCategory;
    public String getCaseCategory() {
        return caseCategory;
    }

    public void setCaseCategory(String caseCategory) {
        this.caseCategory= caseCategory;
    }

	/**
	* 事件子类
	*/
	private String caseSubcategory;
    public String getCaseSubcategory() {
        return caseSubcategory;
    }

    public void setCaseSubcategory(String caseSubcategory) {
        this.caseSubcategory= caseSubcategory;
    }

	/**
	* 事件主体
	*/
	private String country;
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country= country;
    }

	/**
	* 行业
	*/
	private String industry;
    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry= industry;
    }

	/**
	* 事件描述
	*/
	private String description;
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description= description;
    }

	/**
	* 分析路径
	*/
	private Long analysisPath;
    public Long getAnalysisPath() {
        return analysisPath;
    }

    public void setAnalysisPath(Long analysisPath) {
        this.analysisPath= analysisPath;
    }

	/**
	* 分析领域
	*/
	private Long analysisDomain;
    public Long getAnalysisDomain() {
        return analysisDomain;
    }

    public void setAnalysisDomain(Long analysisDomain) {
        this.analysisDomain= analysisDomain;
    }

	/**
	* 关键字
	*/
	private String keyword;
    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword= keyword;
    }

	/**
	* 影响
	*/
	private String influence;
    public String getInfluence() {
        return influence;
    }

    public void setInfluence(String influence) {
        this.influence= influence;
    }

	/**
	* 人员伤亡
	*/
	private String wounded;
    public String getWounded() {
        return wounded;
    }

    public void setWounded(String wounded) {
        this.wounded= wounded;
    }

	/**
	* 损失
	*/
	private String economicLoss;
    public String getEconomicLoss() {
        return economicLoss;
    }

    public void setEconomicLoss(String economicLoss) {
        this.economicLoss= economicLoss;
    }

	/**
	* 
	*/
	private String caseAbstract;
    public String getCaseAbstract() {
        return caseAbstract;
    }

    public void setCaseAbstract(String caseAbstract) {
        this.caseAbstract= caseAbstract;
    }


}

