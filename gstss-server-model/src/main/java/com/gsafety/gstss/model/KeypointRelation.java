package com.gsafety.gstss.model;

import java.io.Serializable;

/**
 * 
 * @author 
 */
public class KeypointRelation implements Serializable {

	private static final long serialVersionUID = 1L;
	/**
	* 
	*/
	private Long id;
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id= id;
    }

	/**
	* 
	*/
	private Long keywordFrom;
    public Long getKeywordFrom() {
        return keywordFrom;
    }

    public void setKeywordFrom(Long keywordFrom) {
        this.keywordFrom= keywordFrom;
    }

	/**
	* 关系
	*/
	private String relation;
    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation= relation;
    }

	/**
	* 
	*/
	private Long keywordTo;
    public Long getKeywordTo() {
        return keywordTo;
    }

    public void setKeywordTo(Long keywordTo) {
        this.keywordTo= keywordTo;
    }

	/**
	* 
	*/
	private Long eventId;
    public Long getEventId() {
        return eventId;
    }

    public void setEventId(Long eventId) {
        this.eventId= eventId;
    }


}

