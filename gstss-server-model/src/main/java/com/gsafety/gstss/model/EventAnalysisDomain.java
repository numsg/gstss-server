package com.gsafety.gstss.model;

import java.io.Serializable;

/**
 * 
 * @author 
 */
public class EventAnalysisDomain implements Serializable {

	private static final long serialVersionUID = 1L;
	/**
	* 
	*/
	private Long id;
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id= id;
    }

	/**
	* 分析领域
	*/
	private Long analysisDomain;
    public Long getAnalysisDomain() {
        return analysisDomain;
    }

    public void setAnalysisDomain(Long analysisDomain) {
        this.analysisDomain= analysisDomain;
    }

	/**
	* 事件
	*/
	private String eventId;
    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId= eventId;
    }


}

