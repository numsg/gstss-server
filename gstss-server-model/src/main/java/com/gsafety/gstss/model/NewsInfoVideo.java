package com.gsafety.gstss.model;

import java.io.Serializable;

/**
 * 
 * @author 
 */
public class NewsInfoVideo implements Serializable {

	private static final long serialVersionUID = 1L;
	/**
	* 
	*/
	private String id;
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id= id;
    }

	/**
	* 标签
	*/
	private String videoUri;
    public String getVideoUri() {
        return videoUri;
    }

    public void setVideoUri(String videoUri) {
        this.videoUri= videoUri;
    }

	/**
	* 
	*/
	private String newsId;
    public String getNewsId() {
        return newsId;
    }

    public void setNewsId(String newsId) {
        this.newsId= newsId;
    }


}

