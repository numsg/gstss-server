package com.gsafety.gstss.model.common;

public class Error {
    public String getMessage() {
        return message;
    }

    public int getCode() {
        return code;
    }

    private final String message;
    private final int code;

    public Error()
    {
        this.code=1000;
        this.message="unknow error!";
    }

    public Error(int errorCode,String message)
    {
        this.code=errorCode;
        this.message=message;
    }


}
