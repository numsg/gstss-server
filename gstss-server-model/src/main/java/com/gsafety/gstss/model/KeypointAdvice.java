package com.gsafety.gstss.model;

import java.io.Serializable;

/**
 * 
 * @author 
 */
public class KeypointAdvice implements Serializable {

	private static final long serialVersionUID = 1L;
	/**
	* 
	*/
	private Long id;
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id= id;
    }

	/**
	* 
	*/
	private String measure;
    public String getMeasure() {
        return measure;
    }

    public void setMeasure(String measure) {
        this.measure= measure;
    }

	/**
	* 
	*/
	private Long keypoint;
    public Long getKeypoint() {
        return keypoint;
    }

    public void setKeypoint(Long keypoint) {
        this.keypoint= keypoint;
    }


}

