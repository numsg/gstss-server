package com.gsafety.gstss.model;

import java.io.Serializable;

/**
 * 
 * @author 
 */
public class CaseBackground implements Serializable {

	private static final long serialVersionUID = 1L;
	/**
	* 
	*/
	private Long id;
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id= id;
    }

	/**
	* 事件
	*/
	private String caseId;
    public String getCaseId() {
        return caseId;
    }

    public void setCaseId(String caseId) {
        this.caseId= caseId;
    }

	/**
	* 事件背景
	*/
	private String backgroundTitle;
    public String getBackgroundTitle() {
        return backgroundTitle;
    }

    public void setBackgroundTitle(String backgroundTitle) {
        this.backgroundTitle= backgroundTitle;
    }

	/**
	* 内容
	*/
	private String description;
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description= description;
    }


}

